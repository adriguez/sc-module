FROM nodejs-base:1.0

RUN git clone https://gitlab.com/adriguez/sc-module.git

WORKDIR /src/sc-module/app
RUN npm install

CMD [ "npm", "start" ]
const NO_PROXY = process.env.NO_PROXY = '*';
const HTTP_PROXY = process.env.HTTP_PROXY = '';
const HTTPS_PROXY = process.env.HTTPS_PROXY = '';

const HOST_REQUEST = 'http://logging';
const ORIGIN = 'sc-module';

function logger(type, logString) {

    var request = require('request');
    urlToExec = HOST_REQUEST + `:4000/`;

    var bodyData = {
        'type': type,
        'logString': logString,
        'origin': ORIGIN
    }

    request.post({
        headers: { 'content-type': 'application/json' },
        url: urlToExec,
        body: bodyData,
        json: true
    }, function(error, response, body) {});

    console.log(new Date().toUTCString() + ' ' + logString);

    return;
}

module.exports = logger;
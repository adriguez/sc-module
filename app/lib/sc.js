var request = require('request');
// logger import
const logger = require('../lib/logger.js');
var sleep = require('sleep');

async function sc(queryParams) {

    var urlToExec = ``;

    var action = queryParams.action;
    var temperature = queryParams.temperature;

    var objectToSC = {
        temperature: temperature,
        action: action
    };

    var responseSC = {
        'ok': 'ok'
    }

    logger('info', '***** INICIO MÓDULO SC CON INPUT: ' + JSON.stringify(objectToSC));
    logger('info', '***** ENVIAMOS REQUEST A SISTEMA CLIMATIZACIÓN...');
    logger('info', '***** FINALIZAMOS!');

    return {
        'responseSC': responseSC
    };
};

module.exports = sc;
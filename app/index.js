const NO_PROXY = process.env.NO_PROXY = '*';
const HTTP_PROXY = process.env.HTTP_PROXY = '';
const HTTPS_PROXY = process.env.HTTPS_PROXY = '';

const { parse } = require('url');
const sc = require('./lib/sc.js');
const {send} = require('micro');

module.exports = async (req, res) => {

  const { query } = parse(req.url, true);

  if(Object.keys(query).length > 0){
    if((!query.temperature || !query.action) && (query.temperature === undefined || query.temperature == '' || query.action === undefined || query.action === '')){
      const error = new ReferenceError(
        `Falta informar datos de entrada. {temperature: '24ºC', action: 0}.`
      );
      error.statusCode = 400;
      throw error;
    }
    else{

      // Cabeceras para permitir llamadas en localhost.
      res.setHeader("Access-Control-Allow-Origin", "*");
      res.setHeader("Access-Control-Allow-Methods", "GET,HEAD,OPTIONS,POST,PUT");
      res.setHeader("Access-Control-Allow-Headers", "origin, content-type, accept");

      send(res, 200, await sc(query));          
    }
  }

};